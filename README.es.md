<div align="center">
    <a href="https://twitter.com/intent/follow?screen_name=kennbroorg">
        <img alt="follow on Twitter" src="https://img.shields.io/twitter/follow/kennbroorg.svg?label=follow%20%40kennbroorg&style=social">
    </a>
</div>

---

<div align="center">
    <a href="https://gitlab.com/kennbroorg/iky-docker/blob/master/README.md">
	<img alt="README English" src="https://img.shields.io/badge/README-English-orange.svg">
    </a>
</div>

---

<div align="center">
    <img alt="Logo" src="https://kennbroorg.gitlab.io/ikyweb/assets/img/Logo-Circular-Docker.png">
</div>

---

# iKy Docker

## Descripción

El proyecto iKy es una herramienta que colecta información a partir de una dirección de e-mail y muestra los resultados en una interface visual

Visite el Gitlab Page del [Projecto](https://kennbroorg.gitlab.io/ikyweb/)

[![Video Demo](https://kennbroorg.gitlab.io/ikyweb/assets/img/iKy-01.png)](https://vimeo.com/326114716 "Video Demo - Click to Watch!") 

[Video Demo](https://vimeo.com/326114716 "Video Demo - Click to Watch!")

## Docker
Docker debe estar instalado

### Construir la imagen
Construir la imagen a partir del dockerfile
```shell
docker build  -t "iky:dockerfile" .
```

### Ejecutar el contenedor
Ejecutar el contenedor desde la imagen creada exponiendo los puertos
```shell
docker run -ti --name iky -p 4200:4200 -p 5000:5000 iky:dockerfile
```
Esperar aproximadamente dos minutos a que los servicios finalicen su inicialización

## Navegador
Abrir el browser en esta [url](http://127.0.0.1:4200) 

## Configurar las API Keys
Una vez que la aplicación esté cargada en el browser, deberá ir a la opción Api Keys y llenar los valores de las APIs que se necesitan

- Fullcontact : Generar las APIs desde [aquí](https://support.fullcontact.com/hc/en-us/articles/115003415888-Getting-Started-FullContact-v2-APIs)
- Twitter : Generar las APIs desde [aquí](https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens.html)
- Linkedin : Solo se debe cargar el usuario y contraseña de su cuenta 

## Otras cosas acerca de Docker

### Ver los logs dentro del contenedor
Para ver los logs dentro del contenedor
```shell
docker exec -i -t iky tail /opt/log/redis.log # Redis LOG
docker exec -i -t iky tail /opt/log/celery.log # Celery LOG
docker exec -i -t iky tail /opt/log/app.log # App LOG
docker exec -i -t iky tail /opt/log/frontend.log # Frontend LOG
```

### Obtener un bash dentro del contenedor
Abrir terminal bash dentro del contenedor
```shell
docker exec -i -t iky /bin/bash
```

### Ver los contenedores
Para visualizar los contenedor que se encuentran corriendo o detenidos
```shell
docker container ps -a
```

### Borrar contenedor
Para borrar un contenedor detenido o corriendo
```shell
docker container rm -f <CONTAINER-ID> 
```

